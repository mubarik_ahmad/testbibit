package com.stockbit.local.dao

import androidx.room.*
import com.stockbit.model.Student

@Dao
interface StudentDao {

    @Insert
    fun insert(student: Student)

    @Update
    fun update(student: Student)

    @Delete
    fun delete(student: Student)

    @Query("SELECT * FROM student")
    fun getAll() : List<Student>

    @Query("SELECT * FROM student WHERE id = :id")
    fun getById(id: Int) : List<Student>
}