package com.stockbit.hiring

import android.opengl.Visibility
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.view.isVisible
import com.google.android.material.textfield.TextInputEditText
import com.stockbit.local.AppDatabase
import com.stockbit.local.dao.StudentDao
import com.stockbit.model.Student

class InputData : Fragment() {

    private lateinit var student: Student
    private lateinit var database: AppDatabase
    private lateinit var dao: StudentDao

    private lateinit var name: TextInputEditText
    private lateinit var address: TextInputEditText
    private lateinit var phone: TextInputEditText

    var isUpdate= false

    private lateinit var buttonSave: Button
    private lateinit var buttonDelete: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_input_data, container, false)
        database = AppDatabase.buildDatabase(view.context)
        dao = database.getStudent()

        name = view.findViewById(R.id.edit_text_name)
        address = view.findViewById(R.id.edit_text_address)
        phone = view.findViewById(R.id.edit_text_phone)
        buttonSave = view.findViewById(R.id.button_save)
        buttonDelete = view.findViewById(R.id.button_delete)

        if (arguments?.getParcelable<Student>("data_student") != null) {
            student = arguments?.getParcelable<Student>("data_student")!!
            name.setText(student.name)
            address.setText(student.address)
            phone.setText(student.phone)
            isUpdate = true
            buttonDelete.isVisible = true
        }


        buttonSave.setOnClickListener{
            val valueName = name.text.toString()
            val valueAddress = address.text.toString()
            val valuePhone = phone.text.toString()

            if(valueName.isEmpty() && valueAddress.isEmpty() && valuePhone.isEmpty()){
                Toast.makeText(view.context, "Note cannot be empty", Toast.LENGTH_SHORT).show()
            }else{
                if(isUpdate){
                    dao.update(Student(id = student.id, name = valueName, address = valueAddress, phone = valuePhone))
                }else{
                    dao.insert(Student(name = valueName, address = valueAddress, phone = valuePhone))
                }
                Toast.makeText(context, "Student saved", Toast.LENGTH_SHORT).show()
            }
        }

        buttonDelete.setOnClickListener{
            dao.delete(student)
            Toast.makeText(context, "Note removed", Toast.LENGTH_SHORT).show()
            isUpdate = false
            clearText()
            buttonDelete.isVisible = false
        }
        return view
    }

    fun clearText() {
        name.setText("")
        address.setText("")
        phone.setText("")
    }

}