package com.stockbit.hiring

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.stockbit.hiring.adapter.StudentAdapter
import com.stockbit.local.AppDatabase
import com.stockbit.model.Student


class ListData : Fragment() {

    private lateinit var inputData: FloatingActionButton
    private lateinit var recyclerView: RecyclerView
    private lateinit var textViewNoteEmpty: TextView
    private lateinit var con: Context
    private lateinit var v: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//
//        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.fragment_list_data, container, false)
        con = view.context;
        v = view

        inputData = view.findViewById(R.id.input_data)
        inputData.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_listData_to_inputData)
        }

        recyclerView = view.findViewById(R.id.recycler_view_main)
        textViewNoteEmpty = view.findViewById(R.id.text_view_note_empty)


        getStudentData()

        return view
    }

    private fun getStudentData(){
        val database = AppDatabase.buildDatabase(con)
        val dao = database.getStudent()
        val listItems = arrayListOf<Student>()
        listItems.addAll(dao.getAll())
        setupRecyclerView(listItems)
        if (listItems.isNotEmpty()){
            textViewNoteEmpty.visibility = View.GONE
        }
        else{
            textViewNoteEmpty.visibility = View.VISIBLE
        }
    }

    private fun setupRecyclerView(listItems: ArrayList<Student>){
        recyclerView.apply {
            adapter = StudentAdapter(listItems, object : StudentAdapter.StudentListener{
                override fun onItemClicked(student: Student) {

                    val bundle = bundleOf("data_student" to student)

                    Navigation.findNavController(v).navigate(R.id.action_listData_to_inputData, bundle)
                }
            })

            layoutManager = LinearLayoutManager(con)
        }
    }

    override fun onResume() {
        super.onResume()
        getStudentData()
    }


}