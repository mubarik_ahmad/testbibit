package com.stockbit.hiring.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stockbit.hiring.R
import com.stockbit.model.Student

class StudentAdapter(
    private val listItem: ArrayList<Student>,
    private val listener: StudentListener
) : RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentAdapter.StudentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_student, parent, false)
        return StudentViewHolder(view)
    }

    override fun onBindViewHolder(holder: StudentAdapter.StudentViewHolder, position: Int) {
        val item = listItem[position]
        holder.textViewName.text = item.name
        holder.textViewAddress.text = item.address
        holder.textViewPhone.text = item.phone
        holder.itemView.setOnClickListener{
            listener.onItemClicked(item)
        }
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    class StudentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var textViewName = itemView.findViewById<TextView>(R.id.text_view_name)!!
        var textViewAddress = itemView.findViewById<TextView>(R.id.text_view_address)!!
        var textViewPhone = itemView.findViewById<TextView>(R.id.text_view_phone)!!
    }

    interface StudentListener {
        fun onItemClicked(student: Student)
    }


}